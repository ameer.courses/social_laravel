<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $posts = Post::all();
        $result = array();
        foreach ($posts as $post){
            $user = User::where('id',$post->user_id)->firstOrFail();
            array_push($result,[
                'post' => $post,
                'user' => $user,
                ]);
        }
        return Response()->json([
            'posts' => $result
            ]);
    }

    public function profile()
    {
        $posts = Post::where('user_id',Auth::user()->id)->get();
        return Response()->json([
            'posts' => $posts,
            'user' => Auth::user()
        ]);
    }

    public function store(Request $request)
    {
        $data = Validator::make(
            $request->all(),
            [
                'description' => 'required|string|max:255',
                'image'=> 'required|image',
            ]
        );

        if($data->fails())
            return Response()->json(['errors'=>$data->errors()]);


        $post = Post::create([
            'user_id' => Auth::user()->id,
            'description' => $request['description']
        ]);

        if ($request->hasFile('image')) {
            // Get filename with extension
            $filenameWithExt = $request->file('image')->getClientOriginalName();

            // Get just the filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            // Get extension
            $extension = $request->file('image')->getClientOriginalExtension();

            // Create new filename
            $filenameToStore = $filename.'_'.time().'.'.$extension;

            // Uplaod image
            $path = $request->file('image')->storeAs('public/users/images', $filenameToStore);

            $post->image = URL::asset('storage/'.'users/images/'.$filenameToStore);
            $post->save();
            // <img src="/storage/users/images/{{$user->id}}">
        }


        return Response()->json(['post'=> $post]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        if($post->user_id != Auth::user()->id)
            return Response()->json(['error'=>'this product is not belong to you.']);
        $post->delete();
        return Response()->json(['message'=>'product deleted successfully.']);
    }
}
