<?php

use App\Http\Controllers\PostController;
use App\Http\Controllers\ReactionController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register',[UserController::class,'register']);
Route::post('/login',[UserController::class,'login']);


Route::group(['middleware'=>['auth:sanctum']],
    function (){
        Route::get('/logout',[UserController::class,'logout']);
        Route::prefix('/post')->group(
            function (){
                Route::post('/add',[PostController::class,'store']);
                Route::get('/profile',[PostController::class,'profile']);
                Route::delete('/{id}',[PostController::class,'destroy']);
                Route::post('/reaction/{id}',[ReactionController::class,'store']);
            }
        );
    }
);

Route::get('/home',[PostController::class,'index']);
